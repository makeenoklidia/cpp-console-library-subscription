#pragma once

#ifndef BOOK_SUBSCRIPTION_H
#define SESSION_REESULT_H

#include "constants.h"
#include <string>

struct date
{
    int day;
    int month;
    int year;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct session_result
{
    person student;
    date examDate;
    int mark;
    char discipline[MAX_STRING_SIZE];
};

#endif
