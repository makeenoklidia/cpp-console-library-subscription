#include <iostream>
#include <iomanip>

#include "file_reader.h"
#include "filter.h"
#include "process.h"

using namespace std;

void output(session_result* result) {
    // ����� ���������� � ���������� ������
    cout << "�������: " << result->student.last_name << " " << result->student.first_name << " " << result->student.middle_name << endl;
    cout << "���� ��������: " << setw(2) << setfill('0') << result->examDate.day << "." << setw(2) << setfill('0') << result->examDate.month << "." << result->examDate.year << endl;
    cout << "����������: " << result->discipline << endl;
    cout << "������: " << result->mark << endl;
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");

    session_result* results[MAX_FILE_ROWS_COUNT];
    int size = 0;

    try {
        read("data.txt", results, size);

        cout << "***** ���������� � ���������� ������ *****\n\n";
        for (int i = 0; i < size; i++) {
            output(results[i]);
        }

        cout << "***** ���������� ����������� ������ *****\n\n";
        int new_size = 0;
        bool (*check_function)(session_result*) = nullptr;
        int option;
        cout << "�������� �������� ����������:\n";
        cout << "1) �� ���������� '������� ��������'\n";
        cout << "2) �� ������ ���� 7\n";
        cout << "3) ����� ������ ���������� � �������� ���������\n";
        cout << "������� �����: ";
        cin >> option;
        cout << endl;

        switch (option) {
        case 1:
            check_function = check_sessionResult_by_discipline;
            break;
        case 2:
            check_function = check_sessionResult_by_mark;
            break;
        case 3:
            int sorting_method;
            int sorting_criterion;
            cout << "�������� ����� ����������:\n";
            cout << "1) ���������� ���������\n";
            cout << "2) ���������� ��������\n";
            cout << "������� �����: ";
            cin >> sorting_method;
            cout << endl;
            cout << "�������� �������� ���������:\n";
            cout << "1) �� ������� ��������\n";
            cout << "2) �� ���������� � ������\n";
            cout << "������� �����: ";
            cin >> sorting_criterion;
            cout << endl;
            if (sorting_method == 1) {
                if (sorting_criterion == 1)
                    insertion_sort(results, size, compare_by_last_name);
                else if (sorting_criterion == 2)
                    insertion_sort(results, size, compare_by_discipline_and_mark);
            }
            else if (sorting_method == 2) {
                if (sorting_criterion == 1)
                    merge_sort(results, 0, size - 1, compare_by_last_name);
                else if (sorting_criterion == 2)
                    merge_sort(results, 0, size - 1, compare_by_discipline_and_mark);
            }
            cout << "***** ��������������� ������ *****\n\n";
            for (int i = 0; i < size; i++) {
                output(results[i]);
            }
            break;
        default:
            throw "������������ ����.";
        }

        if (check_function != nullptr) {
            session_result** filtered_results = filter(results, size, check_function, new_size);
            cout << "***** ��������������� ���������� *****\n\n";
            for (int i = 0; i < new_size; i++) {
                output(filtered_results[i]);
            }
            delete[] filtered_results;
        }

        // ������������ ������
        for (int i = 0; i < size; i++) {
            delete results[i];
        }
    }
    catch (const char* error) {
        cout << "������: " << error << endl;
    }

    return 0;
}