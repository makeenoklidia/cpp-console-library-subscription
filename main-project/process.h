#pragma once
#ifndef PROCESSING_H
#define PROCESSING_H

#include "session_result.h"

// ������� ��� ��������� �� ������� ��������
bool compare_by_last_name(session_result* a, session_result* b);

// ������� ��� ��������� �� ���������� � ������
bool compare_by_discipline_and_mark(session_result* a, session_result* b);

// ������� ���������� ���������
void insertion_sort(session_result* array[], int size, bool (*compare)(session_result*, session_result*));

// ������� ��� ������� ��������
void merge(session_result* array[], int left, int mid, int right, bool (*compare)(session_result*, session_result*));

// ������� ���������� ��������
void merge_sort(session_result* array[], int left, int right, bool (*compare)(session_result*, session_result*));
#endif
