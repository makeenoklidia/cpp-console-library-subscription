#ifndef FILTER_H
#define FILTER_H

#include "session_result.h"

session_result** filter(session_result* array[], int size, bool (*check)(session_result* element), int& result_size);


bool check_sessionResult_by_discipline(session_result* element);



bool check_sessionResult_by_mark(session_result* element);

#endif
