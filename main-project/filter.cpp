#include "filter.h"

//������� ����������
session_result** filter(session_result* array[], int size, bool (*check)(session_result* element), int& result_size) {
	// ��������� ������ ��� ��������� ������ ����������
	session_result** result_array = new session_result * [size];
	result_size = 0;

	// ������� ���� ��������� ��������� �������
	for (int i = 0; i < size; i++) {
		if (check(array[i])) {
			// ���������� ����������� �������� � �������������� ������
			result_array[result_size++] = array[i];
		}
	}
	// ��������� ������ ��� ������ ������� �������
	session_result** final_result_array = new session_result * [result_size];
	for (int i = 0; i < result_size; ++i) {
		final_result_array[i] = result_array[i];
	}

	// ������������ ���������� �������
	delete[] result_array;

	return final_result_array;
}

// ������� �������� �� ���������� "������� ��������"
bool check_sessionResult_by_discipline(session_result* element) {
	return strcmp(element->discipline, "History of Belarus") == 0;
}

// ������� �������� �� ������ ���� 7 ������
bool check_sessionResult_by_mark(session_result* element) {
	return element->mark > 7;
}
