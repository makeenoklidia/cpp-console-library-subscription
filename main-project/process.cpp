#include "process.h"

#include <iostream>
#include <cstring>

bool compare_by_last_name(session_result* a, session_result* b) {
    return strcmp(a->student.last_name, b->student.last_name) < 0;
}

bool compare_by_discipline_and_mark(session_result* a, session_result* b) {
    int discipline_compare = strcmp(a->discipline, b->discipline);
    if (discipline_compare == 0) {
        return a->mark > b->mark;
    }
    return discipline_compare < 0;
}

void insertion_sort(session_result* array[], int size, bool (*compare)(session_result*, session_result*)) {
    for (int i = 1; i < size; ++i) {
        session_result* key = array[i];
        int j = i - 1;
        while (j >= 0 && compare(key, array[j])) {
            array[j + 1] = array[j];
            --j;
        }
        array[j + 1] = key;
    }
}

// ������� ��� ������� ��������
void merge(session_result* array[], int left, int mid, int right, bool (*compare)(session_result*, session_result*)) {
    int n1 = mid - left + 1;
    int n2 = right - mid;

    session_result** L = new session_result * [n1];
    session_result** R = new session_result * [n2];

    for (int i = 0; i < n1; ++i)
        L[i] = array[left + i];
    for (int j = 0; j < n2; ++j)
        R[j] = array[mid + 1 + j];

    int i = 0, j = 0;
    int k = left;
    while (i < n1 && j < n2) {
        if (compare(L[i], R[j])) {
            array[k] = L[i];
            ++i;
        }
        else {
            array[k] = R[j];
            ++j;
        }
        ++k;
    }

    while (i < n1) {
        array[k] = L[i];
        ++i;
        ++k;
    }

    while (j < n2) {
        array[k] = R[j];
        ++j;
        ++k;
    }

    delete[] L;
    delete[] R;
}

// ������� ���������� ��������
void merge_sort(session_result* array[], int left, int right, bool (*compare)(session_result*, session_result*)) {
    if (left < right) {
        int mid = left + (right - left) / 2;

        merge_sort(array, left, mid, compare);
        merge_sort(array, mid + 1, right, compare);

        merge(array, left, mid, right, compare);
    }
}
